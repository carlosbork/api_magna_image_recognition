def analize_requeriments_by_part(part, items_found, type, type_of_table):
    data_svg = {}
    bit_to_send = []
    if type_of_table == "ESTACION 100":
        # --lado largo------------------------------------
        # PR-INSIDE = Grafeno
        # P4R-OUTSIDE = Aerogel
        # --lado corto -----------------------------------
        # P4R-L = TOT LH
        # P4R-R = TOT RH
        # ------------------------------------------------          
        if part == 1 and type == "upper":  
            if "grafeno_of_upper" in items_found:
                data_svg["Grafeno"] = {"nombre": "Grafeno", "valor": "0"}
                bit_to_send.append(1)        
            else:
                data_svg["Grafeno"] = {"nombre": "Grafeno", "valor": "1"}
                bit_to_send.append(0)

            if "aero_gel" in items_found:
                data_svg["Aerogel"] = {"nombre": "Aerogel", "valor" :"0"}
                bit_to_send.append(1)                
            else:
                data_svg["Aerogel"] =  {"nombre": "Aerogel", "valor" : "1"}
                bit_to_send.append(0)
            if "barcode_label_1" in items_found:
                data_svg["QR"] = {"nombre": "QR", "valor":  "0"}
                bit_to_send.append(1)                
            else:
                data_svg["QR"] = {"nombre": "QR", "valor": "1"}
                bit_to_send.append(0)
        if part == 2 and type == "upper":
            if "tape_over_tape_izquierdo" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_LH"] =  {"nombre": "TOT_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}
            if "tape_over_tape_derecho" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}                
            else:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor" :"1"}

        # --lado largo-----------------------------------
        # P4R-OUTSIDE = Aerogel
        # P5R-R = QR RH
        # P5R-L = QR LH
        # --lado corto----------------------------------
        # P4R-R = TOT RH
        # P4R-L = TOT LH
        # P5R-R = NotchTape RH
        # P5R-L = NotchTape LH
        # ------------------------------------------------
        if part == 1 and type == "lower":
            if "aero_gel" in items_found:
                bit_to_send.append(1)
                data_svg["Aerogel"] = {"nombre": "Aerogel", "valor": "0"}                
            else:
                bit_to_send.append(0)
                data_svg["Aerogel"] = {"nombre": "Aerogel", "valor": "1"}
                
            if "barcode_label_2" in items_found:
                bit_to_send.append(1)
                data_svg["Datamatrix"] = {"nombre": "Datamatrix", "valor":  "0"}                
            else:
                bit_to_send.append(0)
                data_svg["Datamatrix"] = {"nombre": "Datamatrix", "valor": "1"}
            if "barcode_label_1" in items_found:
                bit_to_send.append(1)
                data_svg["QR"] = {"nombre": "QR", "valor":  "0"}                
            else:
                bit_to_send.append(0)
                data_svg["QR"] = {"nombre": "QR", "valor": "1"}

        if part == 2 and type == "lower":
            if not "tape_over_tape_derecho" in items_found:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor" :"1"}
            else:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}
            if not "tape_over_tape_izquierdo" in items_found:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}
            else:
                bit_to_send.append(1)
                data_svg["TOT_LH"] =  {"nombre": "TOT_LH", "valor": "0"}
            if not "tape_over_tape_huequito_derecho" in items_found:
                bit_to_send.append(0)
                data_svg["NotchTape_RH"] = {"nombre": "NotchTape_RH", "valor": "1"}
            else:
                bit_to_send.append(1)
                data_svg["NotchTape_RH"] = {"nombre": "NotchTape_RH", "valor": "0"}
            if not "tape_over_tape_huequito_izquierdo" in items_found:
                bit_to_send.append(0)
                data_svg["NotchTape_LH"] = {"nombre": "NotchTape_LH", "valor": "1"}
            else:
                bit_to_send.append(1)
                data_svg["NotchTape_LH"] = {"nombre": "NotchTape_LH", "valor":"0" }          

    if type_of_table == "ESTACION 110":
        # --lado largo-----------------------------------
        # QR LH = Aerogel
        # TOT RH = cinta amarilla lado derecho
        # TOT LH = cinta amarilla lado izquierdo
        # --lado corto----------------------------------
        # Grafeno = Grafeno
        # TOT RH = cinta amarilla lado derecho
        # TOT LH = cinta amarilla lado izquierdo
        # ------------------------------------------------
        if part == 1 and type == "upper":
            if "barcode_label_2" in items_found:
                data_svg["Datamatrix"] = {"nombre": "Datamatrix", "valor":  "0"}
                bit_to_send.append(1)                
            else:
                data_svg["Datamatrix"] = {"nombre": "Datamatrix", "valor": "1"}
                bit_to_send.append(0)
            if "tape_over_tape_derecho" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "1"}
            if "tape_over_tape_izquierdo" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}
        if part == 2 and type == "upper":
            if "grafeno_of_upper" in items_found:
                bit_to_send.append(1)
                data_svg["Grafeno"] = {"nombre": "Grafeno", "valor":  "0"}
            else:
                bit_to_send.append(0)
                data_svg["Grafeno"] = {"nombre": "Grafeno", "valor":  "1"}
            if "tape_over_tape_derecho" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "1"}
            if "tape_over_tape_izquierdo" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}
        # ---lado largo -----------------
        # TOT RH
        # TOT LH
        # ----lado corto -----------------
        # TOT RH
        # TOT LH
        # NotchTape RH
        # NotchTape LH
        #---------------------------------
        if part == 1 and type == "lower":
            if "tape_over_tape_derecho" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "1"}
            if "tape_over_tape_izquierdo" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}       
        
        
        if part == 2 and type == "lower":
            if "tape_over_tape_derecho2" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_RH"] = {"nombre": "TOT_RH", "valor": "1"}
            if "tape_over_tape_izquierdo2" in items_found:
                bit_to_send.append(1)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["TOT_LH"] = {"nombre": "TOT_LH", "valor": "1"}
            if "tape_over_tape_huequito_derecho" in items_found:
                bit_to_send.append(1)
                data_svg["NotchTape_RH"] = {"nombre": "NotchTape_RH", "valor": "0"}                
            else:
                bit_to_send.append(0)
                data_svg["NotchTape_RH"] = {"nombre": "NotchTape_RH", "valor": "1"}
            if "tape_over_tape_huequito_izquierdo" in items_found:
                bit_to_send.append(1)
                data_svg["NotchTap_LH"] = {"nombre": "NotchTape_LH", "valor": "0"}
            else:
                bit_to_send.append(0)
                data_svg["NotchTape_LH"] = {"nombre": "NotchTape_LH", "valor": "1"}
    return data_svg, bit_to_send



