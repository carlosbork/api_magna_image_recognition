from evaluate_piece import analize_requeriments_by_part
from process_image import detect_objects
import cv2
from yolov5.utils.torch_utils import select_device
import pathlib
import sys
from yolov5.models.experimental import attempt_load
import os

if sys.platform.startswith('win'):
    temp = pathlib.PosixPath
    pathlib.PosixPath = pathlib.WindowsPath
color_red = (254, 0, 0)

class_clasifity_model = {
    "Clase 0": "grafeno_of_upper",
    "Clase 1": "aero_gel",
    "Clase 2": "barcode_label_1",
    "Clase 3": "tape_over_tape",
    "Clase 4": "tessa_tape_black",
    "Clase 5": "tessa_tape_black_square",
    "Clase 6": "barcode_label_2",
    "Clase 7": "huequito_en_el_lower",
    "Clase 8": "tape_over_tape_huequito",
    "Clase 9": "bubble_in_aerogel",
}


base_path = "yolov5"
train_path = "runs"
exp_path = "train"
weights_path = "weights"
model_name = "best.pt"
current_directory = os.getcwd()
model_relative_path = os.path.join("yolov5", "runs", "train", "exp2", "weights", "best.pt")
model_absolute_path = os.path.join(current_directory, model_relative_path)
yolo_model = attempt_load(model_absolute_path)




def execute_analysis(frame_original, type_analysis, type_of_table):
    device = select_device()
    coordenadas = None
    items_found = []
    results_to_send = []
    results, frame_redimensionado = detect_objects(frame_original, yolo_model, device=device)
    for label, confidence, bbox in results:
        class_actual = class_clasifity_model.get(label, None)
        x, y, w, h = map(int, bbox)
        x_original = int(x * (frame_original.shape[1] / frame_redimensionado.shape[1]))
        y_original = int(y * (frame_original.shape[0] / frame_redimensionado.shape[0]))
        w_original = int(w * (frame_original.shape[1] / frame_redimensionado.shape[1]))
        h_original = int(h * (frame_original.shape[0] / frame_redimensionado.shape[0]))
        bbox_center_x = x + w // 2
        position = ""
        if bbox_center_x < 540 / 2:
            position = "_izquierdo"
        else:
            position = "_derecho"
        what_detect = class_actual if class_actual in ["aero_gel", "grafeno_of_upper", "barcode_label_1", "barcode_label_2", "tape_over_tape_huequito"] else class_actual + position
        if what_detect == "tape_over_tape_huequito":
            what_detect = "tape_over_tape_huequito_izquierdo" if "tape_over_tape_huequito_derecho" in items_found else "tape_over_tape_huequito_derecho"
        if what_detect == "tape_over_tape_izquierdo":
            what_detect = "tape_over_tape_izquierdo2" if "tape_over_tape_izquierdo" in items_found else "tape_over_tape_izquierdo"
        if what_detect == "tape_over_tape_derecho":
            what_detect = "tape_over_tape_derecho2" if "tape_over_tape_derecho" in items_found else "tape_over_tape_derecho"
        items_found.append(what_detect)
        if class_actual == "barcode_label_2":
            coordenadas = {
                "x":x_original,
                "y":y_original,
                "h":h_original,
                "w":w_original
            }
            # cropped_image = frame_original[y_original:y_original+h_original, x_original:x_original+w_original]
        results_to_send.append({"what_detect": what_detect, "x": x_original, "y": y_original, "h": h_original, "w": w_original})
        # cv2.putText(frame_original, what_detect, (x_original, y_original), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color_red, 4)
    data_svg, bit_to_send = analize_requeriments_by_part(1, items_found, type_analysis, type_of_table)
    data_svg2, bit_to_send2 = analize_requeriments_by_part(2, items_found, type_analysis, type_of_table)
    return data_svg, bit_to_send, data_svg2, bit_to_send2, results_to_send, coordenadas

