import base64
import cv2
import torch
from yolov5.utils.general import non_max_suppression

def resize_frame(frame, width, height):
    return cv2.resize(frame, (width, height))

def detect_objects(frame, model, device):
    frame = resize_frame(frame, 960, 540)
    img = frame[:, :, ::-1].transpose(2, 0, 1).copy()  # Hacer una copia del arreglo antes de realizar las operaciones
    img = torch.from_numpy(img).to(device).float()  # Convertir a tensor y enviar a dispositivo
    img /= 255.0  # Normalizar la imagen
    model.eval()
    with torch.no_grad():
        predictions = model(img.unsqueeze(0))[0]  # Realizar la inferencia en el tensor de la imagen
        detections = non_max_suppression(predictions, conf_thres=0.5, iou_thres=0.5)  # Filtrar detecciones

    results = []
    if detections[0] is not None:
        for detection in detections[0]:
            x1, y1, x2, y2, conf, cls = detection
            label = f"Clase {int(cls)}"
            results.append((label, conf.item(), (x1, y1, x2 - x1, y2 - y1)))
    return results, frame
