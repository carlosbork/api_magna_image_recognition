# Usa la imagen base de Python 3.11
FROM python:3.11

# Establece el directorio de trabajo en /app
WORKDIR /app

# Actualiza la lista de paquetes e instala las dependencias del sistema
RUN apt-get update && \
    apt-get install -y \
    gnupg2 \
    libopencv-dev \
    libglib2.0-0 && \
    rm -rf /var/lib/apt/lists/*

# Copia el archivo de requerimientos al contenedor
COPY requirements.txt .

# Instala las dependencias de Python
RUN pip install --no-cache-dir -r requirements.txt

# Copia el código fuente al contenedor
COPY . .

# Expone el puerto 8012
EXPOSE 8012

# Define el comando por defecto para ejecutar la aplicación
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8012"]
