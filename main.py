from fastapi import FastAPI
from app.api.endpoints import clasify_images

app = FastAPI(title="Clasification Images", version="0.1")

app.include_router(clasify_images.router, prefix="/clasify", tags=["clasify"])

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8014)
