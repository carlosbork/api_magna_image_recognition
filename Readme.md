 Proyecto de Clasificación de Imágenes

Este proyecto implementa un sistema de clasificación de imágenes utilizando un modelo de aprendizaje automático entrenado con TensorFlow y FastAPI para crear un API REST para interactuar con el modelo.


## Instalación

1. Clona este repositorio.
2. Instala las dependencias:
    ```
    pip install -r requirements.txt
    ```

## Uso

1. Ejecuta la aplicación FastAPI:
    ```
    uvicorn main:app --reload --port 8010
    ```
2. Ejecuta la aplicación FastAPI:
    ```
    127.0.0.1:8012/docs
    ```
3. Envía una imagen al endpoint `/classify/` para clasificarla.

## Licencia

Este proyecto está bajo la Licencia de bork.

## Versionamiento

python 3.11
