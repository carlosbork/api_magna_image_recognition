from PIL import Image

from analisis import execute_analysis


def analyze_image_of_a_piece(image):
    try:
        img = Image.fromarray(image)
        result =execute_analysis(img, "lower", "ESTACION 100")        
        return {"result":result}
    except Exception as e:
        return {"error": str(e)}
