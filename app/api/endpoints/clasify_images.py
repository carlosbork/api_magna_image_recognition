from fastapi import UploadFile, File
from PIL import Image
from io import BytesIO
from fastapi import APIRouter
import numpy as np
from PIL import Image
from analisis import execute_analysis
import base64
import cv2
import time

router = APIRouter()


@router.post("/classify_lower_100/")
async def classify_image(image: UploadFile = File(...)):
    try:
        start_time = time.time()
        image_content = await image.read()
        img = Image.open(BytesIO(image_content))
        img_array = np.array(img)
        (
            data_svg,
            bit_to_send,
            data_svg2,
            bit_to_send2,
            results,
            cropped_image,
        ) = execute_analysis(img_array, "lower", "ESTACION 100")
        result = {
            "data_svg": data_svg,
            "bit_to_send": bit_to_send,
            "data_svg2": data_svg2,
            "bit_to_send2": bit_to_send2,
            "frame_original": results,
            "cropped_image": cropped_image,
        }
        end_time = time.time()
        print(f"Tiempo tomado: {end_time - start_time} segundos")
        return result
    except Exception as e:
        return {
            "error": "No se pudo procesar la imagen. Asegúrate de que sea una imagen válida.",
            "detail": str(e),
        }




@router.post("/classify_lower_110/")
async def classify_image(image: UploadFile = File(...)):
    if True:
        image_content = await image.read()
        img = Image.open(BytesIO(image_content))
        img_array = np.array(img)
        (
            data_svg,
            bit_to_send,
            data_svg2,
            bit_to_send2,
            frame_original,
            cropped_image,
        ) = execute_analysis(img_array, "lower", "ESTACION 110")
        _, buffer = cv2.imencode('.png', frame_original)
        frame_base64 = base64.b64encode(buffer).decode('utf-8')
        # _2, buffer2 = cv2.imencode('.png', cropped_image)
        # frame_base642 = base64.b64encode(buffer2).decode('utf-8')
        result = {
            "data_svg": data_svg,
            "bit_to_send": bit_to_send,
            "data_svg2": data_svg2,
            "bit_to_send2": bit_to_send2,
            "frame_original": frame_base64,
            "cropped_image": "",
        }
        return result
    # except Exception as e:
    #     return {
    #         "error": "No se pudo procesar la imagen. Asegúrate de que sea una imagen válida.",
    #         "detail": str(e),
    #     }





@router.post("/classify_upper_100/")
async def classify_image(image: UploadFile = File(...)):
    if True:
        image_content = await image.read()
        img = Image.open(BytesIO(image_content))
        img_array = np.array(img)
        (
            data_svg,
            bit_to_send,
            data_svg2,
            bit_to_send2,
            frame_original,
            cropped_image,
        ) = execute_analysis(img_array, "lower", "ESTACION 100")
        _, buffer = cv2.imencode('.png', frame_original)
        frame_base64 = base64.b64encode(buffer).decode('utf-8')
        # _2, buffer2 = cv2.imencode('.png', cropped_image)
        # frame_base642 = base64.b64encode(buffer2).decode('utf-8')
        result = {
            "data_svg": data_svg,
            "bit_to_send": bit_to_send,
            "data_svg2": data_svg2,
            "bit_to_send2": bit_to_send2,
            "frame_original": frame_base64,
            "cropped_image": "frame_base642",
        }
        return result
    # except Exception as e:
    #     return {
    #         "error": "No se pudo procesar la imagen. Asegúrate de que sea una imagen válida.",
    #         "detail": str(e),
    #     }




@router.post("/classify_upper_110/")
async def classify_image(image: UploadFile = File(...)):
    if True:
        image_content = await image.read()
        img = Image.open(BytesIO(image_content))
        img_array = np.array(img)
        (
            data_svg,
            bit_to_send,
            data_svg2,
            bit_to_send2,
            frame_original,
            cropped_image,
        ) = execute_analysis(img_array, "lower", "ESTACION 110")
        _, buffer = cv2.imencode('.png', frame_original)
        frame_base64 = base64.b64encode(buffer).decode('utf-8')
        # _2, buffer2 = cv2.imencode('.png', cropped_image)
        # frame_base642 = base64.b64encode(buffer2).decode('utf-8')
        result = {
            "data_svg": data_svg,
            "bit_to_send": bit_to_send,
            "data_svg2": data_svg2,
            "bit_to_send2": bit_to_send2,
            "frame_original": frame_base64,
            "cropped_image": "",
        }
        return result
    # except Exception as e:
    #     return {
    #         "error": "No se pudo procesar la imagen. Asegúrate de que sea una imagen válida.",
    #         "detail": str(e),
    #     }



    



@router.get("/test/")
async def test():
    return {"success": True}
